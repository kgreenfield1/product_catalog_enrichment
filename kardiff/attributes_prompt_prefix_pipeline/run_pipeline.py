#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.

"""This file runs the main pipeline."""

import argparse
import logging
import os
import sys
from typing import List, Type
import yaml

import apache_beam as beam
from apache_beam.options import pipeline_options

from src.attributes_prompt_prefix_pipeline import pipeline
from src.common import utils


def parse_arguments(argv: List[str]) -> Type[argparse.Namespace]:
    """Parses and validates command-line arguments.
    Args:
        argv: A list of sys arguments passed using the command-line.

    Returns:
        A tuple with the known arguments and the user pipeline arguments.
    """
    # First, determine whether a config file was specified. If it was, use it to
    # set defaults. If an argument was specified in the config file, then it is
    # not a required command-line argument. If an argument is specified in both
    # the config file and the command-line, the command-line takes precedent.
    config_parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
    )
    config_parser.add_argument(
        "--config_file",
        help="""Local or GCS path to YAML config file. If none specified,
        required arguments will be parsed from the command-line.""",
        type=str,
    )
    args, _ = config_parser.parse_known_args(argv)
    config = {}
    if args.config_file:
        with open(args.config_file) as f:
            config = yaml.safe_load(f)

    parser = argparse.ArgumentParser(
        parents=[config_parser],
        description=__doc__,
    )
    parser.add_argument(
        "--project",
        help="GCP project ID.",
        type=str,
        required=("project" not in config),
    )
    parser.add_argument(
        "--service_account",
        help="""Service account to use to run Dataflow. Default `None`
        which uses user credentials.""",
        type=str,
        default=None,
    )
    parser.add_argument(
        "--subnetwork",
        help="""Subnetwork to run Dataflow on. Default `None` which uses the
        default subnetwork in the project.""",
        type=str,
        default=None,
    )
    parser.add_argument(
        "--src_dataset",
        help="Source BigQuery dataset.",
        type=str,
        required=("src_dataset" not in config),
    )
    parser.add_argument(
        "--src_table",
        help="Source BigQuery table.",
        type=str,
        required=("src_table" not in config),
    )
    parser.add_argument(
        "--dst_dataset",
        help="Destination BigQuery dataset. Must already exist.",
        type=str,
        required=("dst_dataset" not in config),
    )
    parser.add_argument(
        "--dst_table",
        help="Destination BigQuery table. Will be created if not already.",
        type=str,
        required=("dst_table" not in config),
    )
    parser.add_argument(
        "--job_dir",
        help="""Directory for temporary, staging, and intermediate files.
        Must be a GCS bucket if running on the cloud.""",
        type=str,
        required=("job_dir" not in config),
    )
    parser.add_argument(
        "--region",
        help="GCP region to run Dataflow in.",
        type=str,
        required=("region" not in config),
    )
    parser.add_argument(
        "--job_name",
        help="Dataflow job name. Timestamp will be appended.",
        type=str,
        required=("job_name" not in config),
    )
    parser.add_argument(
        "--machine_type",
        help="Dataflow worker machine type. Default `n1-standard-2`.",
        type=str,
        default="n1-standard-2",
    )
    parser.add_argument(
        "--max_num_workers",
        help="""Maximum number of workers allowed in the Dataflow job. Default
        `None` which allows GCP default of 1000.""",
        type=int,
        default=None,
    )
    parser.add_argument(
        "--setup_file",
        help="Path to setup.py file specifying local package dependencies.",
        type=str,
        default="./setup.py",
    )

    parser.add_argument(
        "--prod",
        help="Whether to run the job on GCP (True). Default `False`.",
        action="store_true",
    )
    parser.add_argument(
        "--overwrite_bigquery_table",
        help="Whether to recreate the destination table. Default `False`.",
        action="store_true",
    )

    # Set defaults from config file.
    parser.set_defaults(**config)
    args, _ = parser.parse_known_args(argv)
    # Append current timestamp to job name.
    args.job_name, args.timestamp = utils.get_job_name_with_timestamp(
        args.job_name)
    logging.info(f"Job name with timestamp: {args.job_name}.")

    return args


def get_pipeline_options(
        args: Type[argparse.Namespace]
) -> Type[pipeline_options.PipelineOptions]:
    """Gets pipeline options for the Beam pipeline.

    Args:
        args: Parsed command-line arguments.

    Returns:
        Pipeline options to create the Beam pipeline with.
    """
    # Instantiate dict of options that are always necessary.
    options_dict = {
        "runner": "DirectRunner",
        "project": args.project,
        "temp_location": os.path.join(args.job_dir, "tmp"),
    }
    if not args.prod:
        return pipeline_options.PipelineOptions(flags=[], **options_dict)

    # For prod (dataflow) runs, add necessary Dataflow pipeline options.
    options_dict.update({
        "runner": "DataflowRunner",
        "job_name": args.job_name,
        "staging_location": os.path.join(args.job_dir, "staging"),
        "region": args.region,
        "max_num_workers": args.max_num_workers,
        "setup_file": args.setup_file,
        "service_account_email": args.service_account,
        "subnetwork": args.subnetwork,
        "use_public_ips": False,
    })
    options = pipeline_options.PipelineOptions(flags=[], **options_dict)
    # TODO: Put machine type in options_dict.
    # Not sure why machine type is separated.
    options.view_as(
        pipeline_options.WorkerOptions).machine_type = args.machine_type
    return options


def main():
    """Configures and runs the Apache Beam pipeline."""
    logging.getLogger().setLevel(logging.INFO)
    args = parse_arguments(sys.argv)
    options = get_pipeline_options(args)
    with beam.Pipeline(options=options) as beam_pipeline:
        pipeline.build_pipeline(beam_pipeline, args)


if __name__ == "__main__":
    main()
