#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.
"""BigQuery schema for prompt prefix output table"""

schema = {
    "fields": [
        {
            "name": "product_type",
            "type": "STRING",
            "mode": "REQUIRED"
        },
        {
            "name": "prompt_values",
            "type": "STRING",
            "mode": "REPEATED"
        }
    ]
}
