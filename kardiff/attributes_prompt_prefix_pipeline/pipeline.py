#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.

"""This file is the main file that builds and runs the attribute pipeline. The
pipeline takes metadata from a BigQuery table and creates PALM API prompt
values, and writes the results to a destination table."""

import argparse
from typing import Type

import apache_beam as beam
from apache_beam.io.gcp.internal.clients import bigquery

from src.attributes_prompt_prefix_pipeline import create_attributes
from src.attributes_prompt_prefix_pipeline import output_schema


def make_attribute_query(
    project: str,
    src_dataset: str,
    src_table: str
):
    return f"""
    WITH mapping AS (
      SELECT
        level_1,
        level_2,
        level_3,
        level_4,
        display_name,
        Taxonomy_Key,
        acceptable_values,
        closed_list,
        multi_select,
        acceptable_units,
        example_values
      FROM `{project}.{src_dataset}.{src_table}`
      WHERE
          level_4 IS NOT NULL
      GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
    ), null_product_types AS (
      SELECT
        level_1,
        level_2,
        level_3,
        Taxonomy_Key,
        display_name,
        acceptable_values,
        closed_list,
        multi_select,
        acceptable_units,
        example_values
      FROM `{project}.{src_dataset}.{src_table}`
      WHERE
          level_4 IS NULL
          AND Taxonomy_Key IS NOT NULL
      GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    ), joined AS (
    SELECT
      mapping.level_1,
      mapping.level_2,
      mapping.level_3,
      mapping.level_4 AS product_type,
      null_product_types.display_name,
      null_product_types.Taxonomy_Key AS attribute_key,
      null_product_types.acceptable_values,
      IF(null_product_types.closed_list = "No", False, True) AS closed_list,
      IF(null_product_types.multi_select = "No", False, True) AS multi_select,
      null_product_types.acceptable_units,
      null_product_types.example_values
    FROM null_product_types
    JOIN mapping
    USING (level_3)
    GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
    UNION ALL
    SELECT
      level_1,
      level_2,
      level_3,
      level_4 AS product_type,
      display_name,
      Taxonomy_Key AS attribute_key,
      acceptable_values,
      IF(closed_list = "No", False, True) AS closed_list,
      IF(multi_select = "No", False, True) AS multi_select,
      acceptable_units,
      example_values
    FROM `{project}.{src_dataset}.{src_table}`
    WHERE
        level_4 IS NOT NULL
    GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
    SELECT DISTINCT *
    FROM joined
    WHERE attribute_key IS NOT NULL
    """

def build_pipeline(
    pipeline: Type[beam.Pipeline],
    args: Type[argparse.Namespace],
) -> None:
    """Builds Apache Beam pipeline.

    Args:
        pipeline: Beam pipeline.
        args: User-specified arguments.
    """
    # TODO(kardiff): Add in ignore-list.
    # Input queries
    attributes_query = make_attribute_query(
        project=args.project,
        src_dataset=args.src_dataset,
        src_table=args.src_table
    )
    if not args.prod:
        attributes_query += "LIMIT 20"
        args.dst_table += "_test"
    # Output config
    output_table_spec = bigquery.TableReference(
        projectId=args.project,
        datasetId=args.dst_dataset,
        tableId=args.dst_table,
    )
    if args.overwrite_bigquery_table:
        write_disposition = beam.io.BigQueryDisposition.WRITE_TRUNCATE
    else:
        write_disposition = beam.io.BigQueryDisposition.WRITE_APPEND
    schema = output_schema.schema

    # Beam Pipeline
    attributes = (
        pipeline
        | "Read from BigQuery Attributes table" >> beam.io.ReadFromBigQuery(
            query=attributes_query, use_standard_sql=True)
    )

    _ = (
        attributes
        | "Create Attributes" >> beam.ParDo(
            create_attributes.CreateAttributes())
        | "Group by Product Type" >> beam.GroupBy(
            lambda element: element[1]["product_type"])
        | "Convert grouped attributes from list to dict" >> beam.Map(
            lambda element: (element[0], dict(element[1])))
        | "Create Prefixes" >> beam.ParDo(
            create_attributes.CreatePrefixPrompts())
        | "Prep for BigQuery" >> beam.Map(
            lambda element: {
                "product_type": element[0],
                "prompt_values": element[1]["prompt_values"]
            })
        | "Write to BigQuery" >> beam.io.WriteToBigQuery(
            output_table_spec,
            schema=schema,
            write_disposition=write_disposition,
            create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED)
    )  # yapf: disable
