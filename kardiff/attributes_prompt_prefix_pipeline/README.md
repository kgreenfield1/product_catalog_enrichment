# Attributes Prompt Prefix Pipeline

## Overview
This Dataflow pipeline takes data from a BigQuery dataset as input and outputs a
new table in BigQuery where each row contains [prompt prefix](#what-is-a-prompt-prefix) 
values for a product type.

It orchestrates the following high-level steps: 
1. Reads in the BigQuery source table to extract attributes data.
2. Groups attribute data by product type.
3. Generates prompt prefix for each product type based on the attributes and accepted 
   values
4. Writes prompt prefix values to BigQuery

## What is a Prompt Prefix
A prompt prefix is the top part of the prompt that will be sent to the PALM API. This 
part is generated based on the product type and does not have any information related
to the product item itself

For example in the prompt below, all the text above Product name will be considered the
prompts prefix

```
'A Multipack Quantity found in the Product Name or Long Description below with the only valid options as "", or "N/A" as "multipack_quantity"\nA Count Per Pack found in the Product Name or Long Description below with the only valid options as "", or "N/A" as "count_per_pack"\nA Total Count found in the Product Name or Long Description below with the only valid options as "", or "N/A" as "number_of_pieces"\nA Piece Count found in the Product Name or Long Description below with the only valid options as "", or "N/A" as "piece_count"\nA Product Net Content Parent found in the Product Name or Long Description below with the only valid options as "", or "N/A" as "product_net_content_parent"\nA Product Net Content found in the Product Name or Long Description below with the only valid options as "", or "N/A" as "product_net_content_quantity"\nA Product Net Content UOM found in the Product Name or Long Description below with the only valid options as "Quart", "Fluid Ounces", "Kilogram", "Each", "Pound", "Square Foot", "Gallon", "Foot", "Quart Dry", "Centiliter", "Meter", "Liter", "Case", "Milliliter", "Centimeter", "Pallet/Unit Load", "Pint", "Cubic Foot", "Ounce", "Inch", "Yard", "Gram", or "N/A" as "product_net_content_uom"\nA Net Content Statement found in the Product Name or Long Description below with the only valid options as "", or "N/A" as "net_content_statement"\nA Manufacturer found in the Product Name or Long Description below with the only valid options as "", or "N/A" as "manufacturer"\nAn Occasion found in the Product Name or Long Description below with the only valid options as "", or "N/A" as "occasion"\n--------------\n

Product Name: Loaded Premium Whey Protein with MCTs - Vanilla Peanut Butter (2.1 Lbs. / 27 Servings)\n    Product Type: Protein Supplements\n    Long Description: BUILD. RECOVER. STRENGTH. RYSE LOADED PROTEIN combines premium ingredients with added benefits while simultaneously bringing incredible 5 star reviewed flavors to the table! There is no reason you can\'t have the added benefits of a high quality protein formula without sacrificing taste. Let RYSE LOADED PROTEIN show you the difference! 25g of Whey Protein with whey isolate as its Primary Source Added MCT\'s (Medium Chain Triglycerides) Extremely easy to digest & no stomach bloat afterwards 5 star reviewed flavors across the board PREMIUM INGREDIENTS ONLY Ryse takes a unique approach to the lineup, utilizing the latest science and ingredients backed by clinical evidence. Not only do we use the best patented ingredients that are backed by research, we TEST multiple times. You\'ll always get what the label says and only use the highest quality ingredients, many are patented. By looking at our labels you will see that we cut no corners creating our products and never will, that is our commitment to you!Protein Drink Mix, Vanilla Peanut Butter, Loaded Protein Natural and artificial flavor. Ryse up protein guide: 25 g whey protein per scoop. 16 prebiotic fiber per scoop. 16 premium MCTs per scoop. 25 g premium whey protein per serving. WPI whey isolate as primary protein source. 1 g premium MCTs add fuel to your protein. 1 g organic prebiotic fiber to aid in digestion. Typical Amino Profile: Aspartic acid 2.6, Threonine 1.7, Serine 1.1, Glutamic Acid 4.2, Glycine 0.4, Alanine 1.2, Valine 1.5, Isoleucine 1.6, Leucine 2.6, Tyrosine 0.7, Phenylalanine 0.8, Histidine 0.4, Lysine 2.2, Arginine 0.6, Proline 1.4, Cystine 0.5, Methionine 0.5, Tryptophan 0.5, Total AA (G) 24.5. Premium whey protein with MCTs. Ryse up supplements. 1 Build. 2 Recover. 3 Strength. Advanced benefits of Ryse up loaded protein. www.rysesupps.com. www.customblowmolding.com Facebook. TikTok. Instagram. (at)ryse-supps. Made in the USA with components of domestic and international origin.
```


## Set up config file
Pipeline configurations are specified via a YAML config file. An example for
this pipeline is provided at
[`src/attributes_prompt_prefix_pipeline/config.yaml.example`](config.yaml.example).
Descriptions for each config are provided in the YAML file comments.

Copy the `config.yaml.example` file to `config.yaml` and modify it as needed.

```sh
cp src/attributes_prompt_prefix_pipeline/config.yaml.example src/attributes_prompt_prefix_pipeline/config.yaml
```

The main configs that must change when the pipelines are run in a new GCP
project are:
* `project`
* `service_account`
* `job_dir`
* `subnetwork`
* `source_table`
* `destination_table`
* `errors_table`
> The `destination_table` is the table where the PaLM API's results will be
> written. The `errors_table` is where the exceptions will be written for later
> analysis. The tables do not need to exist (but can exist) before the job is
> run.

### Set up directories
Dataflow jobs require directories in Cloud Storage for temp and staging folders
which are used in intermediate steps. The files for different pipeline runs will
be organized by the directories used by each run.

We assume that you have a Google Cloud Storage (GCS) bucket already created for
the purposes of the Dataflow pipelines.

## Run the pipeline
After setting the config file, the pipeline can be triggered by running the
[`run_pipeline.py`](run_pipeline.py) file, which takes as arguments the YAML
config file path, and a flag indicating whether it is running for production
purposes (`--prod`) or not.

### Run locally
```sh
python3 -m src.attributes_prompt_prefix_pipeline.run_pipeline \
--config_file=src/attributes_prompt_prefix_pipeline/config.yaml
```
> For local runs, the pipeline by default should select fewer images than cloud
> runs to ensure that local machines are not overloaded with data. See
> [`src/attributes_prompt_prefix_pipeline/run_pipeline.py`](run_pipeline.py) for more
> information.

### Run on Dataflow
```sh
python3 -m src.attributes_prompt_prefix_pipeline.run_pipeline \
--config_file=src/attributes_prompt_prefix_pipeline/config.yaml \
--prod
```

