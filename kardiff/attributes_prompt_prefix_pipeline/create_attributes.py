#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.
"""This file creates the attributes and prompt prefix for each product type."""
from typing import Any, Dict, List, Optional, Tuple

import apache_beam as beam


IGNORED_ITEMS = [
    "Additional Image URL",
    "Battery Size",
    "Condition",
    "Key Features",
    "Product Name",
    "Manufacturer Part Number",
    "Main Image URL",
    "Net Content",
    "Net Content Statement",
    "Is Primary Variant",
    "Site Description",
    "Small Parts Warning Code",
    "Swatch Images",
    "Swatch Image URL",
    "Swatch Variant Attribute",
    "Swatch Image URL",
    "Variant Attribute Names",
    "Variant Group ID",
]

START_ATTRIBUTES = []

HALLUCINATING_ATTRIBUTES = [
    "brand",
    "display_technology",
    "manufacturer",
    "multipack_quantity",
    "ingredient_claims",
    "primary_ingredient",
    "product_accessories_included"
]

EXAMPLE_VALUES_ATTRIBUTES = [
    "model"
]

END_ATTRIBUTES = [
    "ingredients",
    "model",
    "features",
    "compatible_cars"
]

DEFAULT_MAX_ATTRIBUTES = 25


def parse_attribute_list(
    item_list: str,
    num_items: Optional[int] = None,
) -> List[str]:
    """Parses attributes list by ; into new list.

    Args:
        item_list: String of list of items to split.
        num_items: Optional integer denoting how many
            items to pick from the list.

    Returns:
        List of strings that are now concatenated by
            comma via the list.
    """
    values = item_list.strip().split(";")
    if num_items is not None:
        # pylint: disable=invalid-unary-operand-type
        values = values[:-num_items]
    values_list = []
    for value in values:
        values_list.append(f"\"{value}\"")
    return values_list


def _create_attributes(
    row: Dict[str, Any],
    ignored_items: List[str],
    example_values_attrs: List[str]
) -> Dict[str, Any]:
    """Creates dictionary of attribute prompts.

    Args:
        row: Dictionary of items, where each key is a field about the attribute,
            such as the `attribute_key`, `product_type`, etc.
        ignored_items: List of strings, which are attribute types that we do
            not need to implement.
    """
    if row["display_name"].strip() not in ignored_items:
        # Parse out accepted_values for closed lists to be a list of
        # strings, where each string is surrounded by quotation marks.
        closed_value_list = None
        if row["acceptable_values"] is not None:
            closed_value_list = parse_attribute_list(
                item_list = row["acceptable_values"])

        ex_values_list = None
        # Parse out example_values. This is helpful for attributes with units,
        # or certain formatting. We take the first four values as some items
        # have much more than we need and we don't want to send too many tokens.
        if (row["attribute_key"] in example_values_attrs
            and row["example_values"] is not None):
            ex_values_list = parse_attribute_list(
                item_list=row["example_values"],
                num_items=4)

        return {
            "product_type": row["product_type"],
            "display_name": row["display_name"].strip(),
            "multi_select": row["multi_select"],
            "attribute_key": row["attribute_key"].strip(),
            "acceptable_values": closed_value_list,
            "closed_list": row["closed_list"],
            "acceptable_units": row["acceptable_units"],
            "example_values": ex_values_list
        }
    else:
        return {}


class CreateAttributes(beam.DoFn):
    """Create dictionaries of attributes for each product type.
    Attributes:
        ignored_items: List of strings, which are keys that we do not need.
    """

    def __init__(self):
        beam.DoFn.__init__(self)
        self.ignored_items = IGNORED_ITEMS
        self.example_values_attrs = EXAMPLE_VALUES_ATTRIBUTES

    def process(
            self,
            row: Dict[str, Any]
    ) -> Optional[Tuple[str, Dict[str, Any]]]:
        """Creates tuple for each attribute.

        Args:
            row: The dictionary of attribute metadata, containing the key,
                whether or not it is a closed-list, etc.

        Yields:
            Tuple pair of attribute name and attribute dictionary that
             has closed list formatted.
        """

        attributes = _create_attributes(
            row=row,
            ignored_items=self.ignored_items,
            example_values_attrs=self.example_values_attrs
        )

        if len(attributes) > 0:
            yield attributes["display_name"], attributes


def add_article(attribute: str, is_multi_select: bool) -> str:
    """Add article to attribute, depending on first letter.

    Args:
        attribute: String representing noun form of attribute.
        is_multi_select: Boolean indicating if attribute is a multi-select or
            single-select.

    Returns:
        String with article in front.
    """
    if is_multi_select is True:
        new_attribute = f"Any {attribute}"
    else:
        vowels = ["a", "e", "i", "o", "u", "y"]
        if attribute[0].lower() in vowels:
            new_attribute = f"An {attribute}"
        else:
            new_attribute = f"A {attribute}"
    return new_attribute


def create_attribute_string(
    attribute_values: Dict[str, Any],
    hallucinating_attributes: List[str],
    example_values_attributes: List[str]) -> str:
    """Creates string for attribute.

    Args:
        attribute_values: Dictionary holding features for the attribute,
            such as multi_select, acceptable_values, etc.

    Returns:
        String with proper article at beginning, potential closed list values
            attached, and suggestion to find attribute in product name or
            description.
    """
    attribute_prefix = add_article(
        attribute=attribute_values["display_name"],
        is_multi_select=attribute_values["multi_select"])

    if attribute_values["acceptable_values"] is not None:
        closed_list_string = ", ".join(attribute_values["acceptable_values"])
        closed_list = (
            f' with the only valid options as {closed_list_string}, or \"N/A\"')
    else:
        closed_list = ""

    if attribute_values["attribute_key"] in hallucinating_attributes:
        hallucinating_context = " and never make up facts"
    else:
        hallucinating_context = ""
    if (attribute_values["attribute_key"] in example_values_attributes
         and attribute_values["example_values"] is not None):
        ex_values = " or ".join(attribute_values["example_values"])
        ex_values_prompt = (
            f' with only valid example patterns as {ex_values} or \"N/A\"')
    else:
        ex_values_prompt = ""

    prompt = (f"\"{attribute_values['attribute_key']}\": {attribute_prefix}")

    if (attribute_values["attribute_key"] not in example_values_attributes
        and attribute_values["acceptable_units"] is not None):
        acceptable_units = attribute_values["acceptable_units"].strip()
        acceptable_units_example = (
        " or ".join([f'"5 {item}"' for item in acceptable_units.split(",")]))

        prompt += (" along with the unit found in the Product Name or "
            f"Product Type or Long Description"
            f" in the format {acceptable_units_example}")
    else:
        prompt += (" found in the Product Name or Product Type or Long "
                   "Description")


    prompt += f"{closed_list}{ex_values_prompt}{hallucinating_context}."


    return prompt


def check_attribute_list_length(
        prompt_list: List[str],
        start_attributes: List[str],
        end_attributes: List[str]) -> str:
    """Creates string from lists of attributes.

    Args:
        prompt_list: List of strings, where each is the specified text for each
            attribute.
        start_attributes: List of strings, where each is an attribute is from
            self.start_attributes.
        end_attributes: List of strings, where each is an attribute is from
            self.end_attributes.

    Returns:
        String of formatted prompt output.
    """
    if len(end_attributes) > 0:
        # We want these specific attributes to be at the end of
        # the prompt. From prompt tuning, we found that they
        # perform better at the end. So we deliberately add them
        # at the end, once we have reached the maximum number
        # of attributes.
        for attribute_prompt in end_attributes:
            prompt_list.append(attribute_prompt)
    if len(start_attributes) > 0:
        # We want these specific attributes to be at the start of
        # the prompt. From prompt tuning, we found that they
        # perform better at the start. So we deliberately add them
        # at the start, once we have reached the maximum number
        # of attributes.
        for attribute_prompt in start_attributes:
            prompt_list.insert(0, attribute_prompt)
    prompt_string = "\n".join(prompt_list)
    return f"""{prompt_string}
--------------"""


def _create_prefix_prompt(
        attribute_tuple: Tuple[str, Dict[str, Dict[str, Any]]],
        start_attributes: List[str],
        end_attributes: List[str],
        hallucinating_attributes: List[str],
        example_values_attributes: List[str],
        max_attributes: int) -> Tuple[str]:
    """Creates prefix prompt for each attribute.

    Args:
        attribute_tuple: Tuple of product_type and attribute dictionary.
        start_attributes: List attributes for the start of the prompt.
            These were found to have better performance at the start
            of the prompts.
        end_attributes: List attributes for the end of the prompt. These were
            found to have better performance at the end of the prompts.
        max_attributes: Maximum number of attributes to add to the PaLM call.
            Because PaLM has a token limit on output/input, we have to restrict
            for both performance reasons as well as tokens, which helps the JSON
            to be properly formatted.

    Returns:
        Tuple of (product_type, dict) where the dictionary contains the prompts
            for each attribute.
    """
    prompt_dict = {"prompt_values": []}
    product_type, attribute_dict = attribute_tuple

    prompt_list = []
    end_attrs = []
    start_attrs = []
    for attribute_values in attribute_dict.values():
        attribute_prompt = create_attribute_string(
            attribute_values=attribute_values,
            hallucinating_attributes=hallucinating_attributes,
            example_values_attributes=example_values_attributes)
        if attribute_values["attribute_key"].strip() in start_attributes:
            start_attrs.append(attribute_prompt)
        elif attribute_values["attribute_key"].strip() in end_attributes:
            end_attrs.append(attribute_prompt)
        else:
            prompt_list.append(attribute_prompt)

        # After creating each prompt, we check to see how many
        # attributes we have currently created. If that number is at
        # least the number of max attributes, then we add it to the list
        # of prompts and start over.
        curr_attr_length = len(prompt_list) + len(end_attrs) + len(start_attrs)

        if curr_attr_length >= max_attributes:
            prompt_string = check_attribute_list_length(
                prompt_list=prompt_list,
                start_attributes=start_attrs,
                end_attributes=end_attrs)
            prompt_dict["prompt_values"].append(prompt_string)
            # Once added, we reset the current `prompt_list` and begin
            # again.
            prompt_list = []
            end_attrs = []
            start_attrs = []
    # Once we have gone through the whole dictionary of all attributes
    # for the product_type, it's possible we haven't yet added the
    # prompts, because there were less than the number of
    # `max_attributes`. In that case, we ned to add them to the output
    # now.
    if len(prompt_list) > 0 or len(end_attrs) > 0 or len(start_attrs) > 0:
        prompt_string = check_attribute_list_length(
            prompt_list=prompt_list,
            start_attributes=start_attrs,
            end_attributes=end_attrs)
        prompt_dict["prompt_values"].append(prompt_string)
    return product_type, prompt_dict


class CreatePrefixPrompts(beam.DoFn):
    """CreatePrefixPrompts is used to create prompts per product type.

    Attributes:
        max_attributes: The maximum number of attributes to include. Because
            PaLM limit the number of characters, we are limited in how many we
            can send at a time.
    """

    # TODO(tidoto): Find a more intelligent way to pick the max, might be by
    # character limit, since we know we are limited.
    def __init__(self, max_attributes=DEFAULT_MAX_ATTRIBUTES):
        """CreatePrefixPrompts is used to create prompts per product type."""
        beam.DoFn.__init__(self)
        self.max_attributes = max_attributes
        self.start_attributes = START_ATTRIBUTES
        self.end_attributes = END_ATTRIBUTES
        self.hallucinating_attributes = HALLUCINATING_ATTRIBUTES
        self.example_values_attributes = EXAMPLE_VALUES_ATTRIBUTES

    def process(
            self,
            attribute_tuple: Tuple[str, Dict[str, Dict[str, Any]]]
    ) -> Tuple[str, Dict[str, List[str]]]:
        """Creates lists of strings to be used for prompts of each product_type.

        Args:
            attribute_tuple: Tuple, where the key is the `product_type`
                and the value is a dictionary about elements of the attribute in
                the format `{'Brand': {'attribute': 'Brand', 'plural': False,
                'attribute_key': 'brand', 'acceptable_values':
                None, 'closed_list': False}}`.
        """
        product_type, prompt_dict = _create_prefix_prompt(
            attribute_tuple=attribute_tuple,
            start_attributes=self.start_attributes,
            end_attributes=self.end_attributes,
            hallucinating_attributes=self.hallucinating_attributes,
            example_values_attributes=self.example_values_attributes,
            max_attributes=self.max_attributes,
        )
        yield product_type, prompt_dict
