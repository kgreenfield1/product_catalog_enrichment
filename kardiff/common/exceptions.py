#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.
"""This file defines the custom exception classes."""

class NullResponseFromPalmException(Exception):
    """Exception for when PaLM API returns a null or empty response."""
    def __init__(self, message):
        self.message = message
        super().__init__(self.message)


class MaxRequestsReachedException(Exception):
    """Exception for when max number of requests is reached."""
    def __init__(self, message):
        self.message = message
        super().__init__(self.message)

class JsonException(Exception):
    """Exception for when JSON conversion fails.
    """
    def __init__(self, message):
        self.message = message
        super().__init__(self.message)
