#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.

"""This file contains shared helper functions for the pipelines."""

import datetime
import csv
import logging
import numpy as np
import os
import pandas as pd
from typing import Any, Dict, List, Optional, Tuple

from google.cloud import bigquery
from google.cloud import storage


def get_job_name_with_timestamp(job_name: str) -> Tuple[str, datetime.datetime]:
    """Appends the timestamp to the given job name.

    Args:
         job_name: The name of the job.

    Returns:
        A 2-tuple (job name with timestamp suffix, timestamp object).
    """
    timestamp = datetime.datetime.now()
    return f"{job_name}-{timestamp.strftime('%Y%m%d%H%M%S')}", timestamp


def parse_gcs_uri(uri: str) -> Tuple[str, str]:
    """Returns the given GCS URI's bucket and blob separately as a tuple.

    This function assumes that the given URI has been validated for GCS URI
    formatting (i.e., it begins with "gs://"). If no additional directory was
    specified, the second item in the tuple is `None`.

    Args:
        uri: GCS URI to parse.

    Returns:
        2-tuple of format (bucket name, blob name).
    """
    split = uri.split("/", 3)
    bucket_name = split[2]
    blob_name = split[3] if len(split) > 3 else None
    return bucket_name, blob_name

def get_storage_client(
        project_id: str,
        credentials_path: Optional[str]) -> storage.Client:
    """This function returns a configured GCS client.

    Returns:
        A configured GCS client.
    """
    if credentials_path is None:
        return storage.Client(project=project_id)
    return storage.Client.from_service_account_json(credentials_path)


def download_data_from_gcs(
    project_id: str,
    gcs_source_data: str,
    local_data_directory: str,
    credentials_path: Optional[str] = None,
    extension: str = ""
) -> List[str]:
    """This function downloads the source data from GCS.

    Args:
        project_id: String of GCP project ID.
        gcs_source_data: The file that will be downloaded.
        local_data_directory: The directory on the local file system where the
            files will be downloaded.
        credentials_path: The path where the credentials are stored.
        extension: An optional extension to append at the end of each
            downloaded file.

    Returns:
        The paths file where the data is stored.
    """
    logging.debug(f"Downloading data from {gcs_source_data} to"
                  f" {local_data_directory}.")
    bucket_name, blob_name = parse_gcs_uri(gcs_source_data)
    # Initialize a client.
    storage_client = get_storage_client(project_id, credentials_path)
    # Create a bucket object for our bucket.
    bucket = storage_client.get_bucket(bucket_name)
    blobs = bucket.list_blobs(prefix=blob_name)
    results = []

    for blob in blobs:
        try:
            # Download the file to a destination.
            filename = blob.name.split("/")[-1]
            dest_path = os.path.join(
                local_data_directory, f"{filename}{extension}")
            blob.download_to_filename(dest_path)
            results.append(dest_path)
            logging.debug(f"Downloaded {blob.name} to {dest_path}.")
        # pylint: disable=broad-except
        except Exception as e:
            logging.error(f"Downloading {blob.name} to {dest_path} failed.")
            logging.error(e)
    return results


def convert_csv_to_dict_list(file_path: str) -> List[Dict[str, Any]]:
    """Converts CSV to a list of dictionaries.

    Args:
        file_path: String to where file is located.

    Returns:
        List of dictionaries, where each row is a dictionary.
    """
    with open(file_path, "r") as csvfile:
        # Create a CSV reader object
        reader = csv.DictReader(csvfile)
        return list(reader)

def execute_bq_query(
    project: str,
    query: str
) -> List[Dict[str, Any]]:
    """Executes query in BigQuery given query string.

    Args:
        project: GCP project where the query should be run.
        query: String of query to send to BigQuery.

    Returns:
        List of dictionaries holding the results, where each
            dictionary is a row in the result.
    """
    bq_client = bigquery.Client(project=project)
    query_results = bq_client.query(query).result()
    results = []
    for row in query_results:
        results.append(dict(row.items()))
    return results

def create_percentiles_table(column: pd.core.series.Series) -> pd.DataFrame:
    """Creates percentile table count given pandas column.

    Args:
        column: Pandas column to calculate percentile.
    Returns:
        Pandas dataframe with percentile distribution of the given column.
    """
    # Calculate percentiles.
    percentiles = np.arange(0, 1.1, 0.1).round(2)

    # Count values in each percentile range.
    hist, _ = np.histogram(column, bins=10)

    # Create a pandas DataFrame to store results
    percentile_df = pd.DataFrame({"Percentile Range": [
        f"{percentiles[i]}-{percentiles[i + 1]}"
        for i in range(len(percentiles) - 1)],"Count": hist})
    return percentile_df


def read_file(path: str) -> str:
    """Returns the contents of the given local or GCS file as string.

    Args:
        path: Path to the file either locally or on GCS.

    Returns:
        The file's contents as str.
    """
    if path.startswith("gs://"):
        client = storage.Client()
        bucket_name, blob_name = parse_gcs_uri(path)
        bucket = client.get_bucket(bucket_name)
        blob = bucket.get_blob(blob_name)
        return blob.download_as_string()
    else:
        with open(path, "r", encoding="utf-8") as f:
            return f.read()

