# Catalog Enrichment
This is the main repository for Catalog Enrichment engagement. This
README provides Google Cloud environment setup instructions and an overview of
the directories. For instructions on how to run individual pipelines and models,
see the READMEs in the respective subdirectories. For context and additional
details, please see the accompanying Technical Design Document (TDD).


## Google Cloud setup
> Subsequent READMEs assume these setup steps are met. Ensure that they are
> before proceeding.

### Prerequisites
If you are using [Cloud
Shell](https://cloud.google.com/shell/docs/launching-cloud-shell) from the Cloud
Console, which has all the prerequisites below pre-installed, you may skip to
the "Environment Setup" section. The steps below will outline how to install the
prerequisites on a local or virtual machine.

1. You should have Python 3 installed on your system.
2. We manage Python packages with
   [virtualenv](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/)
   to avoid installing them globally. If it's not installed already, you can
   install it via pip: `python3 -m pip install --user virtualenv` (use `apt-get
   install python3-venv` on Linux).
3. We use the `gcloud` command to interact with Google Cloud Platform. It should
   be preinstalled on GCP VMs. If you working locally, you may need to install
   it (find the latest version
   [here](https://cloud.google.com/sdk/docs/install-sdk#installing_the_latest_version)).
   If it's the first time you're installing `gcloud`, follow the instructions to
   initialize it
   [here](https://cloud.google.com/sdk/docs/install-sdk#initializing_the).
4. We use the `docker` command to build images needed to deploy Dataflow pipeline 
   [templates](https://cloud.google.com/dataflow/docs/concepts/dataflow-templates)

### Environment Setup
Set up Google Cloud Platform credentials and install dependencies:
```sh
# Set project as an environment variable.
export GOOGLE_CLOUD_PROJECT=<Your project ID>
# Login with user credentials.
gcloud auth login
# Set application-default credentials for the environment.
gcloud auth application-default login
# Set current project.
gcloud config set project $GOOGLE_CLOUD_PROJECT
```
> Note that credentials do expire. You may need to rerun this step every 24
> hours.


```
> Unless explicitly specified, code is run from this directory.

Set up a Python virtual environment to localize dependencies:
```sh
# Create venv.
python3 -m venv venv
# Activate venv.
source venv/bin/activate
# Install requirements.
pip install -r requirements.txt
```

The following Google Cloud services are required to run the source code. Enable
them:
```sh
gcloud services enable aiplatform.googleapis.com
gcloud services enable artifactregistry.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable dataflow.googleapis.com
```

## Using this repo

### Source code ([`src/`](src/))
The following pipelines and models are defined inside of [`src`](src).

### Pipelines
- [`attributes_prompt_prefix_pipeline/`](src/attributes_prompt_prefix_pipeline/): Dataflow pipeline  which takes data from a BigQuery dataset as input and outputs a new table in BigQuery where each row contains [prompt prefix](src/attributes_prompt_prefix_pipeline/README.md#what-is-a-prompt-prefix) values for a product type. It orchestrates the following high-level steps:
   1. Reads in the BigQuery source table to extract attributes data.
   2. Groups attribute data by product type.
   3. Generates prompt prefix for each product type based on the attributes and accepted 
   values
   4. Writes prompt prefix values to BigQuery
- [`data_preparation/`](src/data_preparation/): Dataflow pipeline which separates files in GCS and uploads them to BigQuery so that they can be used later by the [PaLM pipeline](src/palm_pipeline/README.md).
- [`eval_pipeline/`](src/eval_pipeline/): Dataflow pipeline which reads an output file from BigQuery and creates CSV files for the QA team to use for evaluation.
- [`palm_pipeline/`](src/palm_pipeline/): Dataflow pipeline which takes data from a BigQuery dataset as input and outputs a table in BigQuery where each row represents a product item along with metadata retrieved from the [PaLM API](https://cloud.google.com/vertex-ai/docs/generative-ai/text/text-overview). It orchestrates the following high-level steps: 
   1. Reads joined product and prompt prefix value data, which is unnested by prompt prefix values, from BigQuery.
   2. Generates prompts based on a product's description and the prompt prefix of the product type.
   3. Requests metadata (attributes) from Vertex by sending the prompts using the PaLM API.
   4. Groups and combines attributes per product item.
   5. Stores the attributes acquired from PaLM for each product item in a BigQuery table.
   6. Stores errors obtained while generating prompts or sending requests to PaLM in a BigQuery table.

### Notebooks ([`notebooks/`](src/notebooks))
The following Jupyter Notebooks are provided in notebooks:
- [`pipeline_schedule.ipynb`](src/notebooks/pipeline_schedule.ipynb): Templatizes and schedules the [PaLM pipeline](src/palm_pipeline/README.md).
- [`prompting_design.ipynb`](src/notebooks/prompting_design.ipynb):
- [`results_evaluation.ipynb`](src/notebooks/results_evaluation.ipynb):

# Testing
Tests are run automatically by our CI pipeline defined in `.gitlab-ci.yml`.

## Linting
The following command lints all python files.

```sh
pylint --jobs=0 $(git ls-files '*.py')
```

## Unit Tests
Unit tests can be run with the following command:
```sh
pytest -n auto tests/unit
```

A coverage report for a module can be generated by adding the `--cov` flag.
```sh
pytest -n auto tests/unit \
  --cov=src \
  --cov-report term-missing
```
