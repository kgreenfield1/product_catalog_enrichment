#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.
"""This file defines the class that will generate predictions field."""

import copy
from typing import Any, Dict, Iterable, List, Tuple

import apache_beam as beam


IGNORED_VALUES = ["N/A", "", None]

class GeneratePrediction(beam.DoFn):
    """A DoFn that calls that Generates the Predictions field and formats
    elements as TSV."""
    def create_prediction_dict(
        self,
        attribute_list: List[Dict[str, str]]
    )-> str:
        """Reformats attribute list for GCS consumption.

        Args:
            attribute_list: List of attribute key-value pair dictionaries.

        Returns:
            Dictonary with each attribute has a value with the format
                {"prediction":["attribute_value"]}.
        """
        prediction_dict = {}
        for attribute in attribute_list:
            # Only send values to the API if populated.
            if attribute["value"] not in IGNORED_VALUES:
                # TODO(kardiff): Fix output so only multi-select is parsed.
                # Try to convert to list if needed
                val_lst = attribute["value"].split(",")
                val_lst = [val.strip() for val in val_lst]
                prediction_dict[attribute["key"]] = {
                    "prediction": val_lst}
        return prediction_dict

    def format_to_tsv(self, input_dict: Dict[str, Any])-> str:
        """Separates values by tab.

        Args:
            input_dict: Dictionary with keys to be written to GCS.

        Returns:
            Tab separated string of the values for fields(keys) to be
                written to GCS.
        """
        return "\t".join(str(value) for value in input_dict.values())

    def process(
        self,
        element: Tuple[str, Dict[str, Any]],
    ):
        """Filters and reformats data in TSV to be written to GCS.

        Args:
            element: Tuple pair of item id and dictionary of product
                metadata.

        Returns:
            Tab separated string of the values for fields(keys) to be
                written to GCS.
        """
        product_dict = copy.deepcopy(element[1])
        json_schema = {
            "item_id": product_dict["catlg_item_id"],
            "gtin": product_dict["gtin"],
            "wpid": product_dict["wpid"],
            "pt": product_dict["product_type"],
            "pred": self.create_prediction_dict(product_dict["attributes"])
        }
        yield self.format_to_tsv(json_schema)


class ExtractAttributes(beam.DoFn):
    """DoFn to extract attribute keys from dictionary."""

    def process(
        self, attribute_list: List[Dict[str, Any]]
    ) -> Iterable[List[str]]:
        """
        Extracts out all attribute keys from LLM.

        It also filters out attributes for which the LLM failed
        to return an answer and gave back 'N/A', etc.

        Args:
            List of dictionaries from the LLM in the format
                of {'key': 'brand', 'value': 'GE'}

        Yields:
            List of all attribute keys found by the LLM.
        """
        attribute_key_list = []
        for attribute in attribute_list:
            # Only send values to the API if populated.
            if attribute["value"] not in IGNORED_VALUES:
                attribute_key_list.append(attribute["key"])
        yield attribute_key_list
