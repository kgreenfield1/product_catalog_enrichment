#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.

"""This file is the main file that builds and runs the Beam pipeline. The
pipeline takes metadata from a BigQuery table, batches them, sends them to the
PaLM API, and writes the results back to a destination table."""

import argparse
import json
import os
from typing import Type

import apache_beam as beam
from apache_beam.io.gcp.internal.clients import bigquery

from src.palm_pipeline import combine_attributes
from src.palm_pipeline import create_prompt
from src.palm_pipeline import generate_prediction
from src.palm_pipeline import send_to_palm
from src.palm_pipeline import schema


LOCAL_RUN_TABLE_SUFFIX = "_test"


def build_pipeline(
    pipeline: Type[beam.Pipeline],
    args: Type[argparse.Namespace],
) -> None:
    """Builds Apache Beam pipeline.

    Args:
        pipeline: Beam pipeline.
        args: User-specified arguments.
    """
    # Input queries
    query = f"""
    SELECT
        p.catlg_item_id,
        p.ptg,
        p.prod_type_nm AS product_type,
        p.prod_nm AS product_name,
        p.product_short_description AS short_description,
        p.product_long_description AS long_description,
        p.main_image_url,
        p.wpid,
        p.gtin,
        prompt_value
    FROM
        `{args.project}.{args.src_dataset}.{args.product_table}` AS p
    JOIN
        `{args.project}.{args.src_dataset}.{args.prompt_values_table}` AS pv
    ON p.prod_type_nm = pv.product_type,
    UNNEST(pv.prompt_values) AS prompt_value
    WHERE batch_id = {args.batch_id}
    """
    if not args.prod and not args.flex_template:
        query += "LIMIT 15"
        args.dst_table += LOCAL_RUN_TABLE_SUFFIX
        args.err_table += LOCAL_RUN_TABLE_SUFFIX
        args.timestamp_table += LOCAL_RUN_TABLE_SUFFIX
        args.config_table += LOCAL_RUN_TABLE_SUFFIX
    # Output config
    output_table_spec = bigquery.TableReference(
        projectId=args.project,
        datasetId=args.dst_dataset,
        tableId=args.dst_table)
    # error_table_spec = bigquery.TableReference(
    #     projectId=args.project,
    #     datasetId=args.dst_dataset,
    #     tableId=args.err_table)
    # timestamp_table_spec = bigquery.TableReference(
    #     projectId=args.project,
    #     datasetId=args.dst_dataset,
    #     tableId=args.timestamp_table)
    # config_table_spec = bigquery.TableReference(
    #     projectId=args.project,
    #     datasetId=args.dst_dataset,
    #     tableId=args.config_table,
    # )
    if args.overwrite_bigquery_table:
        write_disposition = beam.io.BigQueryDisposition.WRITE_TRUNCATE
    else:
        write_disposition = beam.io.BigQueryDisposition.WRITE_APPEND
    temp_gcs_location = os.path.join(args.job_dir, "tmp")
    # gcs_output_dir = os.path.join(
    #     args.job_dir,
    #     "predictions",
    #     args.job_name,
    #     "output"
    # )

    query_results = (
        pipeline
        | "Read prompt prefix values from BigQuery" >> beam.io.ReadFromBigQuery(
            query=query,
            use_standard_sql=True,
            project=args.project,
            gcs_location=temp_gcs_location))

    # prod_type_list = (
    #     query_results
    #     | "Extract product types" >> beam.Map(lambda row: row["product_type"])
    #     # pylint:disable=no-value-for-parameter
    #     | "Deduplicate PTs" >> beam.Distinct()
    #     | "Combine PTs" >> beam.combiners.ToList()
    # )

    created_prompts, created_prompt_errors = (
        query_results
        | "Add timestamp column" >> beam.Map(
            lambda element: {**element, "timestamp": args.timestamp})
        | "Create prompt" >> beam.ParDo(
            create_prompt.CreatePrompt()).with_outputs(
            "error",
            main="success"))
    palm_responses, palm_errors = (
        created_prompts
        | "Send to PaLM" >> beam.ParDo(send_to_palm.RemoteLLMInference(
            project=args.project, region=args.region)).with_outputs(
            "error",
            main="success")
    )
    # output = (
    #     palm_responses
    #     | "Add attributes" >> beam.Map(send_to_palm.add_attributes_to_item)
    #     | "Key products" >> beam.Map(lambda item: (item["catlg_item_id"], item))
    #     | "Combine attributes per item" >> beam.CombinePerKey(
    #         combine_attributes.CombineAttributesAndPrompts()))

    # # Output API times to BigQuery for QPM tracking.
    # _ = (
    #     palm_responses
    #     | "Prepare for BigQuery" >> beam.Map(lambda elem: ({
    #         "catlg_item_id": elem[0]["catlg_item_id"],
    #         "prompt": elem[0]["prompt"],
    #         "output": json.dumps(elem[1]),
    #         "job_name": args.job_name,
    #         "request_time": elem[2]}))
    #     | "Write request times to BigQuery" >> beam.io.WriteToBigQuery(
    #         timestamp_table_spec,
    #         write_disposition=write_disposition,
    #         schema=schema.timestamp_schema,
    #         create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #         custom_gcs_temp_location=temp_gcs_location
    #     )
    # )
    # _ = (
    #     (created_prompt_errors, palm_errors)
    #     | "Flatten" >> beam.Flatten()
    #     | "Add job_name column" >> beam.Map(
    #         lambda element: {**element, "job_name": args.job_name})
    #     | "Write errors to BigQuery" >> beam.io.WriteToBigQuery(
    #         error_table_spec,
    #         schema=schema.error_schema,
    #         write_disposition=write_disposition,
    #         create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #         custom_gcs_temp_location=temp_gcs_location
    #     )
    # )

    # enriched_attribute_list = (
    #     output
    #     | "Remove key" >> beam.Map(lambda item: item[1])
    #     | "Extract attribute dict" >> beam.Map(
    #         lambda item: item["attributes"])
    #     | "Get list of attributes found" >> beam.ParDo(
    #         generate_prediction.ExtractAttributes())
    #     | "Flatten attribute list" >> beam.FlatMap(list)
    #     # pylint:disable=no-value-for-parameter
    #     | "Get only unique values" >> beam.Distinct()
    #     | "Combine attributes into one list" >> beam.combiners.ToList()
    # )

    # # Output config table.
    # _ = (
    #     prod_type_list
    #     | "Get config values" >> beam.ParDo(
    #         send_to_palm.create_job_config_schema,
    #         args.timestamp,
    #         args.job_name,
    #         args.batch_id,
    #         args.product_table,
    #         args.prompt_values_table,
    #         gcs_output_dir,
    #         f"{args.project}.{args.dst_dataset}.{args.dst_table}",
    #         beam.pvalue.AsSingleton(enriched_attribute_list))
    #     | "Create job config table" >> beam.io.WriteToBigQuery(
    #         config_table_spec,
    #         schema=schema.config_schema,
    #         write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
    #         create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #         custom_gcs_temp_location=temp_gcs_location))

    # # Write out results to BigQuery.
    # _ = (
    #     output
    #     | "Prep for BigQuery" >> beam.Map(lambda item: item[1])
    #     | "Write to BigQuery" >> beam.io.WriteToBigQuery(
    #         output_table_spec,
    #         schema=schema.output_schema,
    #         write_disposition=write_disposition,
    #         create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #         custom_gcs_temp_location=temp_gcs_location,
    #        additional_bq_parameters={
    #            "timePartitioning": {
    #                 "type": "HOUR",
    #                 "field": "timestamp"
    #             }
    #         }
    #     )
    # )  # yapf: disable
