#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.

"""This file runs the main pipeline."""

import argparse
import logging
import os
import sys
from typing import List, Type
import yaml

import apache_beam as beam
from apache_beam.options import pipeline_options

from src.palm_pipeline import pipeline
from src.common import utils


def _str_to_bool(v: str) -> bool:
    """Returns boolean representation of string.

    Args:
        v: String value to convert.

    Returns:
        Boolean version of string.
    """
    if isinstance(v, bool):
        return v
    if v.lower() == "true":
        return True
    if v.lower() == "false":
        return False
    else:
        return argparse.ArgumentTypeError(
            "Value can only be true/false")


def parse_arguments(argv: List[str]) -> Type[argparse.Namespace]:
    """Parses and validates command-line arguments.
    Args:
        argv: A list of sys arguments passed using the command-line.

    Returns:
        A tuple with the known arguments and the user pipeline arguments.
    """
    # First, determine whether a config file was specified. If it was, use it to
    # set defaults. If an argument was specified in the config file, then it is
    # not a required command-line argument. If an argument is specified in both
    # the config file and the command-line, the command-line takes precedent.
    config_parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
    )
    config_parser.add_argument(
        "--config_file",
        help="""Local or GCS path to YAML config file. If none specified,
        required arguments will be parsed from the command-line.""",
        type=str,
    )
    args, _ = config_parser.parse_known_args(argv)
    config = {}
    if args.config_file:
        config = yaml.safe_load(utils.read_file(path=args.config_file))

    parser = argparse.ArgumentParser(
        parents=[config_parser],
        description=__doc__,
    )
    parser.add_argument(
        "--project",
        help="GCP project ID.",
        type=str,
        required=("project" not in config),
    )
    parser.add_argument(
        "--service_account",
        help="""Service account to use to run Dataflow. Default `None`
        which uses user credentials.""",
        type=str,
        default=None,
    )
    parser.add_argument(
        "--subnetwork",
        help="""Subnetwork to run Dataflow on. Default `None` which uses the
        default subnetwork in the project.""",
        type=str,
        default=None,
    )
    parser.add_argument(
        "--src_dataset",
        help="Source BigQuery dataset.",
        type=str,
        required=("src_dataset" not in config),
    )
    parser.add_argument(
        "--product_table",
        help="Source BigQuery table.",
        type=str,
        required=("product_table" not in config),
    )
    parser.add_argument(
        "--prompt_values_table",
        help="Atrributes with prompt value BigQuery table.",
        type=str,
        required=("prompt_values_table" not in config),
    )
    parser.add_argument(
        "--dst_dataset",
        help="Destination BigQuery dataset. Must already exist.",
        type=str,
        required=("dst_dataset" not in config),
    )
    parser.add_argument(
        "--dst_table",
        help="Destination BigQuery table. Will be created if not already.",
        type=str,
        required=("dst_table" not in config),
    )
    parser.add_argument(
        "--err_table",
        help="Error BigQuery table. Will be created if not already.",
        type=str,
        required=("err_table" not in config),
    )
    parser.add_argument(
        "--timestamp_table",
        help="""Table for where to store request times for PaLM. Used for
        benchmarking QPM.""",
        type=str,
        required=("timestamp_table" not in config)
    )
    parser.add_argument(
        "--config_table",
        help="""Table for storing job-related configs, such as job_timestamp,
        prompts used, source input data, etc.""",
        type=str,
        required=("config_table" not in config)
    )
    parser.add_argument(
        "--job_dir",
        help="""Directory for temporary, staging, and intermediate files.
        Must be a GCS bucket if running on the cloud.""",
        type=str,
        required=("job_dir" not in config),
    )
    parser.add_argument(
        "--region",
        help="GCP region to run Dataflow in.",
        type=str,
        required=("region" not in config),
    )
    parser.add_argument(
        "--job_name",
        help="Dataflow job name. Timestamp will be appended.",
        type=str,
        required=("job_name" not in config),
    )
    parser.add_argument(
        "--machine_type",
        help="Dataflow worker machine type. Default `n1-standard-2`.",
        type=str,
        default="n1-standard-2",
    )
    parser.add_argument(
        "--max_num_workers",
        help="""Maximum number of workers allowed in the Dataflow job. Default
        `None` which allows GCP default of 1000.""",
        type=int,
        default=None,
    )
    parser.add_argument(
        "--setup_file",
        help="Path to setup.py file specifying local package dependencies.",
        type=str,
        default="./setup.py",
    )
    parser.add_argument(
        "--batch_id",
        help="""Batch ID representing which run the data should process.""",
        required=("batch_id" not in config)
    )
    parser.add_argument(
        "--prod",
        help="Whether to run the job on GCP (True). Default `False`.",
        type=_str_to_bool,
        default=False,
    )
    parser.add_argument(
        "--overwrite_bigquery_table",
        help="Whether to recreate the destination table. Default `False`.",
        action="store_true")
    parser.add_argument(
        "--flex_template",
        help="""Whether the pipeline has been triggered by a flex template or
 not. Default False.""",
        type=_str_to_bool,
        default=False)


    # Set defaults from config file.
    parser.set_defaults(**config)
    args, _ = parser.parse_known_args(argv)
    # Append current timestamp to job name
    args.job_name, args.timestamp = utils.get_job_name_with_timestamp(
        args.job_name)
    logging.info(f"Job name with timestamp: {args.job_name}.")

    return args


def get_pipeline_options(
        args: Type[argparse.Namespace]
) -> Type[pipeline_options.PipelineOptions]:
    """Gets pipeline options for the Beam pipeline.

    Args:
        args: Parsed command-line arguments.

    Returns:
        Pipeline options to create the Beam pipeline with.
    """
    # For flex templates launch, ensure that pipeline options are not set
    # in the code. Pipeline options are set from the gcloud flex-template
    # run command. Setting pipeline options for flex templates lead to
    # failure to start errors.
    # References
    # - https://cloud.google.com/dataflow/docs/guides/troubleshoot-templates
    # - https://cloud.google.com/sdk/gcloud/reference/dataflow/flex-template/run
    if args.flex_template:
        return pipeline_options.PipelineOptions()
    # Instantiate dict of options that are always necessary.
    options_dict = {
        "runner": "DirectRunner",
        "project": args.project,
        "temp_location": os.path.join(args.job_dir, "tmp"),
        "staging_location": os.path.join(args.job_dir, "staging"),
    }
    if not args.prod:
        return pipeline_options.PipelineOptions(flags=[], **options_dict)
    # For prod (Dataflow) runs, add necessary Dataflow pipeline options.
    options_dict.update({
        "runner": "DataflowRunner",
        "job_name": args.job_name,
        "region": args.region,
        "max_num_workers": args.max_num_workers,
        "setup_file": args.setup_file,
        "service_account_email": args.service_account,
        "subnetwork": args.subnetwork,
        "experiments": ["disable_limit_resizing_by_cpu_util"]
        # "use_public_ips": False
    })
    options = pipeline_options.PipelineOptions(flags=[], **options_dict)
    options.view_as(
        pipeline_options.WorkerOptions).machine_type = args.machine_type
    return options


def main():
    """Configures and runs the Apache Beam pipeline."""
    logging.getLogger().setLevel(logging.INFO)
    args = parse_arguments(sys.argv)
    options = get_pipeline_options(args)
    with beam.Pipeline(options=options) as beam_pipeline:
        pipeline.build_pipeline(beam_pipeline, args)


if __name__ == "__main__":
    main()
