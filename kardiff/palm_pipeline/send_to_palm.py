#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.
"""This file defines the classes that will send the data to the PaLM API."""

import copy
import datetime
import json
import logging
import time
from typing import Any, Dict, Iterable, List, Tuple, Union

import apache_beam as beam
from google.api_core import exceptions
import vertexai
from vertexai.language_models import TextGenerationModel
from vertexai.preview.language_models import CodeGenerationModel

from src.common.exceptions import JsonException
from src.common.exceptions import MaxRequestsReachedException
from src.common.exceptions import NullResponseFromPalmException

# We seem to get null back if we do not start with an empty line.
LLM_CONTEXT = ("\nSYSTEM: Your answer must be referenceable in the text"
" that we are giving you in the Product name or Product Type or Long"
" Description. Write a JSON document that includes the following"
" attributes based on the provided information below. Do not include any"
" other attributes than the ones requested below. If an attribute cannot"
' be found return "N/A" in double quotes for that attribute. Make sure'
" all quotation marks are closed. If they are not closed, add a single"
" blackslash like '/'. If units are needed, include them.")


CODE_BISON_CONTEXT = ("Turn the following into a valid JSON object. Make sure"
" all JSON string values are in double quotes and N/A is inside double quotes"
' like \"N/A\". If there are any unenclosed quotes, add a backslash.\n')

MAX_REQUESTS = 5


class RemoteLLMInference(beam.DoFn):
    """A DoFn that calls the PaLM API.

    Attributes:
        project: GCP project ID.
        context: Any context to attach to the LLM.
        region: String location of where to host GCP resources.
    """
    def __init__(
        self,
        project: str,
        region: str,
        context=LLM_CONTEXT,
        max_requests=MAX_REQUESTS
    ):
        beam.DoFn.__init__(self)
        self.project = project
        self.context = context
        self.location = region
        self.max_requests = max_requests
        self.text_parameters = {
            "temperature": 0.0,
            "max_output_tokens": 1024,
            "top_p": 0.95,
            "top_k": 40
        }
        self.code_parameters = {
            "temperature": 0.0,
            "max_output_tokens": 1024
        }

        self.palm_quota_count = beam.metrics.Metrics.counter(
            self.__class__, "palm_quota_count")
        self.api_exception_count = beam.metrics.Metrics.counter(
            self.__class__, "api_exception_count")
        self.null_exception_count = beam.metrics.Metrics.counter(
            self.__class__, "null_exception_count")
        self.deadline_exceeded_count = beam.metrics.Metrics.counter(
            self.__class__, "deadline_exceeded_count")
        self.text_bison_success_count = beam.metrics.Metrics.counter(
            self.__class__, "text_bison_success_count")
        self.code_bison_success_count = beam.metrics.Metrics.counter(
            self.__class__, "code_bison_success_count")

    def setup(self):
        vertexai.init(project=self.project, location=self.location)
        self.text_model = TextGenerationModel.from_pretrained("text-bison@001")
        self.code_model = CodeGenerationModel.from_pretrained("code-bison@001")

    def handle_quota_reached(self) -> None:
        """Handles quota exceeded errors."""
        self.palm_quota_count.inc()
        logging.warning("Quota reached, waiting 60s...")
        time.sleep(60)

    def handle_api_exception(self) -> None:
        """Handles API exceptions (e.g., ServiceUnavailable)."""
        self.api_exception_count.inc()
        logging.warning(
            "ServiceUnavailable exception caught, retrying immediately...")

    def handle_null_response(self, prompt: str) -> None:
        """Handles exception if response is null."""
        self.null_exception_count.inc()
        message=f"Returned result was null for prompt: {prompt}"
        logging.error(message)
        raise NullResponseFromPalmException(message)

    def handle_deadline_exceeded(self) -> None:
        """Handles exception if deadline is exceeded."""
        self.deadline_exceeded_count.inc()
        logging.warning("Deadline exceeded, retrying immediately...")

    def process(self, element: Dict[str, Any]):
        """Calls the LLM and attaches the returned attributes to the product.

        Args:
            element: Dictionary product metadata.

        Yields:
            Tuple of product information with attributes now
                included.
        """
        product_dict = copy.deepcopy(element)
        product_dict["attributes"] = {}

        try:
            enriched_product_tuple = self.get_llm_result(
                prompt=product_dict["prompt"],
                element=product_dict)

            yield enriched_product_tuple
        # pylint: disable=broad-except
        except Exception as err:
            logging.error(
                f"Unable to get attributes for elem:{product_dict}")
            logging.error(err)
            prompt = f"""{self.context}

input: 
{product_dict["prompt"]}
output:
{{
"""
            output = {
                "catlg_item_id": product_dict["catlg_item_id"],
                "prompt": prompt,
                "timestamp": product_dict["timestamp"],
                "error": type(err).__name__,
                "error_message": str(err),
                "beam_stage": "Send to PaLM",
            }
            yield beam.pvalue.TaggedOutput("error", output)

    def get_llm_result(
            self,
            prompt: str,
            element: Dict[str, str]
    ) -> Tuple[Dict[str, str], Dict[str, str]]:
        """Calls the LLMs and returns formatted as a JSON.

        Args:
            prompt: The text prompt for a given product.
            element: Dictionary with product info.

        Returns:
            Tuple of two dictionaries. The first dictionary is
                the product info. The second dictionary is the
                attribute information.
        """
        full_prompt=f"""{self.context}

input: 
{prompt}
output:
{{
"""
        attribute_response, request_time = self.make_palm_call(
            prompt=full_prompt,
            parameters=self.text_parameters,
            model=self.text_model
        )
        attribute_json = self.convert_to_json(attribute_response)
        elem_with_full_prompt = copy.deepcopy(element)
        elem_with_full_prompt.update({
            "prompt": full_prompt
        })
        return elem_with_full_prompt, attribute_json, request_time

    def make_palm_call(
            self,
            prompt: str,
            parameters: Dict[str, Any],
            model: Union[vertexai.language_models.TextGenerationModel,
                         vertexai.preview.language_models.CodeGenerationModel]
        ) -> str:
        """Handles VertexAI PaLM API call.

        Args:
            prompt: The text prompt to send to the model.
            parameters: Dictionary of parameters for PaLM, such as top_p, etc.
            model: The actual model resource to call, either text or code.

        Returns:
            String response from API.
        """
        for _ in range(self.max_requests):
            try:
                start_time = datetime.datetime.now()
                time.sleep(2.5)
                # response = model.predict(
                #     prompt,
                #     **parameters
                # ).text
                response = "l"
                end_time = datetime.datetime.now()
                request_time = str(end_time - start_time)
                if len(response.strip()) == 0:
                    self.handle_null_response(prompt=prompt)
                else:
                    # Send metrics to Dataflow
                    if model == self.text_model:
                        self.text_bison_success_count.inc()
                    elif model == self.code_model:
                        self.code_bison_success_count.inc()
                return response, request_time
            except exceptions.ResourceExhausted:
                self.handle_quota_reached()
            except exceptions.ServiceUnavailable:
                self.handle_api_exception()
            except exceptions.DeadlineExceeded:
                self.handle_deadline_exceeded()
        try:
            start_time = datetime.datetime.now()
            response = model.predict(
                    prompt,
                    **parameters).text
            end_time = datetime.datetime.now()
            request_time = str(end_time - start_time)

            return response, request_time
        except Exception as err:
            # pylint: disable=line-too-long
            msg = f"Max number of retries({self.max_requests}) reached. Error: {type(err).__name__}"
            raise MaxRequestsReachedException(msg) from err

    def convert_to_json(self, initial_response: str) -> Dict[str, str]:
        """Converts PaLM output to JSON.

        Args:
            response: The response from the PaLM to be formatted.

        Returns:
            Dictionary of original response in JSON format.
        """
        # Sometimes, text-bison decides to add ```json <>``` to the code.
        # In those cases, we just remove all backticks and the word 'json'.
        response = initial_response.replace("```json", "").replace("```", "")

        # Escape any unclosed string literals
        try:
            response = escape_unclosed_quotes(response)
        # Other ways to parse JSON are also tried below.
        # pylint: disable=broad-except
        except Exception as e:
            logging.error(e)
            logging.error(
                f"Parsing unclosed literal failed: {initial_response}")
            # pylint: disable=unnecessary-pass
            pass
        response = response.replace(": N/A",": \"N/A\"")

        # TODO(): Verify if we should be hard-coding this, or not.
        # In certain cases, the PaLM API does not add a " at the end. When this
        # happens, just add the end quotes.
        if not response.endswith("}"):
            if not response.endswith('"}'):
                response += '"}'
            else:
                response += "}"

        # Try to convert response from API into a JSON. If it does not work,
        # then ask Codey to format it to JSON.
        try:
            return json.loads(response)
        except json.decoder.JSONDecodeError:
            prompt = f"{CODE_BISON_CONTEXT} {response}"
            response, _ = self.make_palm_call(
                prompt=prompt,
                parameters=self.code_parameters,
                model=self.code_model
            )
            response = response.replace(": N/A",": \"N/A\"")
            try:
                return dict(response)
            except ValueError as err:
                logging.error(err)
                msg = f"Converting to JSON failed for: {response}"
                raise JsonException(msg) from err


def escape_unclosed_quotes(response: str) -> str:
    """Escapes unclosed quotes for JSON parsing.

    Args:
        response: String with unclosed quotes.

    Returns:
        String with quotes escaped with a backslash.
    """
    # If value can be parsed as JSON, return as-is.
    try:
        json.loads(response)
        return response
    except ValueError:
        response_dict = {}
        # See if it starts with {. If it does, remove it. Because
        # we tell text-bison to start with {, sometimes it does not
        # add it in the response.
        if response.startswith("{"):
            response = response[1:]

        # See if it ends with {. If it does, remove that.
        if response.endswith("}"):
            response = response[:-1]
        # Split the string on the commas. Sometimes,
        # it's returned formatted with each item on a
        # new line. Sometimes it's on the same line.
        if len(response.split(",\n")) > 1:
            items = response.split(",\n")
        # Next we try to check if it's formatted to be
        # all one line. In the event that it is, we need to verify
        # that there is more than one element on the line. Otherwise,
        # it's possible that it's just one element with a "," in the value.
        elif (len(response.split(", ")) > 1 and
              len(response.strip().splitlines()) > 1 and
                response.count(":") > 1):
            items = response.split(", ")
        # In the last case, we assume it's a one-line JSON. We remove any
        # whitespace and any extra brackets that may have passed through.
        else:
            items = [response.strip().replace("}", "").replace("{", "")]
        for item in items:
            # Split the item on the first colon seen.
            key, value = item.split(":", 1)
            # We remove the quotes from beginning and end of key. This
            # also ensures N/A are parsed correctly if they are not
            # enclosed by double quotes.
            key = remove_start_leading_char(key.strip(), '"')
            value = remove_start_leading_char(value.strip(), '"')

            # Escape unclosed quote literals in the value.
            value = value.replace('"', '\"')
            # Add the key and value to the dictionary.
            response_dict[key] = value
        return json.dumps(response_dict)


def remove_start_leading_char(text: str, char: str) -> str:
    """Removes start and leading character from string.

    Args:
        text: String to remove chars from.
        char: Character to search from and remove.

    Returns:
        String without designated character.
    """
    if text.startswith(char):
        text = text[1:]
    if text.endswith(char):
        text = text[:-1]
    return text


def add_attributes_to_item(
    product_info: Tuple[Dict[str, Any], Dict[str, str], str]
)-> Dict[str, Any]:
    """Adds attribute key-value pairs and deletes prompt-related keys from
    an item dictionary.

    Args:
        product_info: Tuple with [Dictionary of product metadata,
            attribute_dict] where attribute_dict is a dictionary
            of attribute key-value pairs.

    Returns:
        Dictionary holding product information with the attribute key-value
            pairs added and prompt, body and prompt_value keys deleted.
    """
    product_dict, attribute_dict, _ = product_info
    copied_product_dict = copy.deepcopy(product_dict)
    if attribute_dict is not None:
        copied_product_dict.update({
            "attributes": attribute_dict})
        copied_product_dict.pop("body")
        copied_product_dict.pop("prompt_value")
    return copied_product_dict


def create_job_config_schema(
        prod_type_list: List[str],
        timestamp: datetime.datetime,
        job_name: str,
        batch_id: int,
        products_table: str,
        prompt_table: str,
        gcs_output_dir: str,
        bq_dst_table: str,
        attribute_list: List[str]
) -> Iterable[Dict[str, Any]]:
    """Creates job configuration schema for storing in BigQuery.

    Args:
        prod_type_list: List of product types used by the
            pipeline.
        timestamp: Time that Dataflow job began.
        job_name: Name of Dataflow job.
        batch_id: The ID that the data ran on.
        products_table: The source table of items that was used.
        prompt_table: The source table holding the prompt values
            for each product_type.
        gcs_output_dir: String location of where files will end
            up in GCS.
        bq_dst_table: The destination table of items that were
            enriched
        attribute_list: List of attributes that the pipeline
            was able to infer.

    Returns:
        Dictionary of key-value pairs where key is the metadata
            key and value is the relevant metadata for this job.
    """
    yield {
        "job_timestamp": timestamp,
        "job_name": job_name,
        "text_bison_context": LLM_CONTEXT,
        "code_bison_context": CODE_BISON_CONTEXT,
        "batch_id": batch_id,
        "products_table": products_table,
        "prompt_table": prompt_table,
        "gcs_output_dir": gcs_output_dir,
        "bq_dst_table": bq_dst_table,
        "prod_type_list": [{"name": pt} for pt in prod_type_list],
        "attribute_list": [{"name": attr} for attr in attribute_list]
    }
