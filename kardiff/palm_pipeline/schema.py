#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.
"""BigQuery schema for palm pipeline output table."""

output_schema = {
    "fields": [
        {
            "name": "catlg_item_id",
            "type": "INTEGER",
        },
        {
            "name": "ptg",
            "type": "STRING",
        },
        {
            "name": "product_type",
            "type": "STRING",
        },
        {
            "name": "product_name",
            "type": "STRING",
        },
        {
            "name": "short_description",
            "type": "STRING",
        },
        {
            "name": "long_description",
            "type": "STRING",
        },
        {
            "name": "main_image_url",
            "type": "STRING",
        },
        {
            "name": "total_description",
            "type": "STRING",
        },
        {
            "name": "wpid",
            "type": "STRING",
        },
        {
            "name": "gtin",
            "type": "STRING",
        },
        {
            "name": "attributes",
            "type": "RECORD",
            "mode": "REPEATED",
            "fields": [
                {
                    "name": "key",
                    "type": "STRING"
                },
                {
                    "name": "value",
                    "type": "STRING"
                }
            ]
        },
        {
            "name": "prompts",
            "type": "STRING",
            "mode": "REPEATED"
        },
        {
            "name": "timestamp",
            "type": "TIMESTAMP",
        },
    ]
}

error_schema = {
    "fields": [
        {
            "name": "job_name",
            "type": "STRING",
        },
        {
            "name": "catlg_item_id",
            "type": "INTEGER",
        },
        {
            "name": "prompt",
            "type": "STRING",
        },
        {
            "name": "timestamp",
            "type": "TIMESTAMP",
        },
        {
            "name": "error",
            "type": "STRING",
        },
        {
            "name": "error_message",
            "type": "STRING",
        },
        {
            "name": "beam_stage",
            "type": "STRING",
        },
    ]
}

timestamp_schema = {
    "fields": [
        {
            "name": "catlg_item_id",
            "type": "INTEGER",
        },
        {
            "name": "prompt",
            "type": "STRING",
        },
        {
            "name": "output",
            "type": "STRING",
        },
        {
            "name": "job_name",
            "type": "STRING"
        },
        {
            "name": "request_time",
            "type": "STRING",
        }]

}


config_schema = {
    "fields": [
        {
            "name": "job_timestamp",
            "type": "TIMESTAMP",
        },
        {
            "name": "job_name",
            "type": "STRING",
        },
        {
            "name": "text_bison_context",
            "type": "STRING",
        },
        {
            "name": "code_bison_context",
            "type": "STRING",
        },
        {
            "name": "batch_id",
            "type": "INTEGER",
        },
        {
            "name": "products_table",
            "type": "STRING",
        },
        {
            "name": "prompt_table",
            "type": "STRING",
        },
        {
            "name": "gcs_output_dir",
            "type": "STRING"
        },
        {
            "name": "bq_dst_table",
            "type": "STRING"
        },
        {
            "name": "prod_type_list",
            "type": "RECORD",
            "mode": "REPEATED",
            "fields": [{
                "name": "name",
                "type": "STRING"
            }]
        },
        {
            "name": "attribute_list",
            "type": "RECORD",
            "mode": "REPEATED",
            "fields": [
                  {
                    "name": "name",
                    "type": "STRING"
                },
            ]
        }
    ]
}
