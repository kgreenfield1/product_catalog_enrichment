#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.
"""This file contains helpers for combining attributes and prompts per item."""

import copy

import apache_beam as beam


class CombineAttributesAndPrompts(beam.CombineFn):
    """A CombineFn that combines attributes and prompts per item."""
    def create_accumulator(self):
        """Initializes an accumulator for attributes, prompts and product dict.

        Returns:
            A tuple of an empty dict, empty list and an empty dict."""
        return {}, [], {}

    def add_input(self, accumulator, product_dict):
        """Combines the accumulator with the product dictionary.

        Args:
            accumulator: Tuple of combined values and values needed for
                combination.
            product_dict: Dictionary of product item.

        Returns:
            An updated accumulator tuple."""
        total_attributes, prompt_list, _ = copy.deepcopy(accumulator)
        total_attributes.update(product_dict["attributes"])
        prompt_list.append(product_dict["prompt"])
        return total_attributes, prompt_list, copy.deepcopy(product_dict)

    def merge_accumulators(self, accumulators):
        """Merges the multiple accumulators that are processed in parrallel
        into a single accumulator.

        Args:
            accumulators: Tuple of combined values and values needed for
                combination.

        Returns:
            A tuple of the updated accumulator containing combined attributes,
                combined prompts and a single product dict."""
        list_of_total_attributes, \
        list_of_total_prompts, \
        product_dicts = copy.deepcopy(zip(*accumulators))
        merged_attributes_dict = {}
        for total_attributes in list_of_total_attributes:
            merged_attributes_dict.update(total_attributes)
        merged_prompt_list = []
        for prompt_list in list_of_total_prompts:
            merged_prompt_list += prompt_list
        # Any product dict could be returned because all of the values
        # (except prompt & attribute) are the same for a unique item id.
        item_info = copy.deepcopy(product_dicts[0])
        return merged_attributes_dict, merged_prompt_list, item_info

    def extract_output(self, accumulator):
        """Extracts the combined product dict that will be outputted by
        the CombineFn.

        Args:
            accumulator: Tuple of combined values and values needed for
                combination.

        Returns:
            Dictionary of unique product item with combined attributes and
                prompts."""
        total_attributes, prompt_list, product_dict = copy.deepcopy(accumulator)
        product_dict["attributes"] = [
            {
                "key": key,
                "value": str(value)
            } for key, value in total_attributes.items()
        ]
        product_dict["prompts"] = prompt_list
        product_dict.pop("prompt")
        return product_dict
