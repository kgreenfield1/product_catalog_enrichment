#  Copyright 2023 Google LLC. This software is provided as-is, without warranty
#  or representation for any use or purpose. Your use of it is subject to your
#  agreement with Google.
"""This file contains helpers for creating the prompt."""

from bs4 import BeautifulSoup
import logging
from typing import Any, Dict

import apache_beam as beam


def remove_leading_trailing_quotes(metadata: str) -> str:
    """Remove quotes from begining and end of string.

    Args:
        metadata: String to remove quotation mark from.

    Returns:
        String without quotation marks.
    """
    if metadata is None:
        return metadata
    return metadata.strip('"').strip()


def get_text_from_html(metadata: str) -> str:
    """Removes HTML tags from text, if present in metadata field.

    Args:
        metadata: String field, which may contain HTML tags.

    Returns:
        String with HTML tags removed.
    """
    if metadata is None:
        return metadata

    text_without_html_tags = BeautifulSoup(metadata, "html.parser").get_text(
        separator=" ").strip()
    # Sometimes, BeautifulSoup reads in text with characters such as '&lt;' or
    # 'b&gt;' which correspond to HTML tags <b> and <br>. In this case, the
    # first call just converts the &lt; to <b>. Because of this, we perform
    # an additional extraction to then get rid of the <b> created from step 1.
    text_without_tag_2 = BeautifulSoup(
        text_without_html_tags, "html.parser").get_text(
        separator=" ").strip()
    # Remove large brackets.
    text = text_without_tag_2.replace("\u3010", "(").replace("\u3011", ")")
    # Replace x's as these may convey dimensions.
    text = text.replace("\u00d7", "x")
    # Sometimes, there are times when there are Unicode characters such as ×,
    # which is not ensure_ascii, will return the Unicode symbol.
    string_enc = text.encode("ascii", "ignore")
    return string_enc.decode()


def format_metadata(metadata: str) -> str:
    """Formats metadata by removing quotes and HTML tags from string.

    Args:
        metadata: String input to reformat.

    Returns:
        String without quotes or HTML tags.
    """
    metadata_without_tags = get_text_from_html(metadata)
    return remove_leading_trailing_quotes(metadata_without_tags)


def add_body(row: Dict[str, Any]) -> Dict[str, str]:
    """Processes the incoming item and returns a body prompt

    Args:
        row: Dictionary of the item to format strings.

    Returns:
        Dictionary with formatted body added into row.
    """
    # TODO(kardiff): Decide if we need to include all of the elements in the
    # dictionary still, or remove unneeded elements and just return the body.
    keys = [
        "product_name", "product_type", "short_description", "long_description"]
    for metadata_key in keys:
        row[metadata_key] = format_metadata(metadata=row[metadata_key])

        if not row[metadata_key]:
            row[metadata_key] = ""

    row["total_description"] = (f"{row['short_description']} "
                                f"{row['long_description']}")

    row["body"] = f"""Product Name: {row["product_name"]}
Product Type: {row["product_type"]}
Long Description: {row["total_description"]}"""
    return row


def get_prompts(
    element_dict: Dict[str, Any],
    prompt: str,
) -> Dict[str, Any]:
    """Creates full prompts for a given item.

    Args:
        element_dict: The item to create the prompts for.
        feature_list: The dictionary of all attribute key-value prompts,
            organized by product type.

    Returns:
        Tuple of (catlg_item_id, product_dictionary) metadata.
    """
    joined_attribute_string = "\n".join(
        [prompt, element_dict["body"]])
    product_dict = element_dict.copy()
    product_dict["prompt"] = joined_attribute_string
    # TODO(kardiff): Decide if we need to return all of the items in the
    # product_dict or if we can remove some of them to save on memory.

    return product_dict


class CreatePrompt(beam.DoFn):
    """CreatePrompt is used to create prompts per prompt prefix values."""

    def process(self, elem: Dict[str, Any]):
        """Creates prompt for each product per prompt prefix values.

        Args:
            row: Dictionary of product items.

        Yields:
            Product dictionary with all product info and new item with
                prompt values.
        """
        try:
            row = add_body(row=elem)
            yield get_prompts(element_dict=row, prompt=row["prompt_value"])
        # pylint: disable=broad-except
        except Exception as err:
            logging.error(
                f"Unable to create prompt attributes for elem:{elem}")
            logging.error(err)
            output = {
                "catlg_item_id": elem["catlg_item_id"],
                "timestamp": elem["timestamp"],
                "error": type(err).__name__,
                "error_message": str(err),
                "beam_stage": "Create Prompt",
            }
            yield beam.pvalue.TaggedOutput("error", output)
